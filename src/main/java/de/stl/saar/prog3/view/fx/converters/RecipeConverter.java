package de.stl.saar.prog3.view.fx.converters;

import de.stl.saar.prog3.factories.FxModelFactory;
import de.stl.saar.prog3.factories.ModelFactory;
import de.stl.saar.prog3.model.interfaces.Recipe;
import de.stl.saar.prog3.view.fx.model.RecipeFx;

public final class RecipeConverter {
	private RecipeConverter() {
	}

	public static Recipe toRecipe(RecipeFx recipeFx) {
		final long recipeId = recipeFx.getRecipeId();
		final String recipeName = recipeFx.getRecipeName();
		final int personCount = recipeFx.getPersonCount();
		final int healthRating = recipeFx.getHealthRating();
		final boolean favorized = recipeFx.getFavorized();
		final String directions = recipeFx.getDirections();
		final int time = recipeFx.getTime();

		final Recipe recipe = ModelFactory.createRecipe(recipeId, recipeName, healthRating, personCount, favorized,
				directions, time);
		return recipe;
	}

	public static RecipeFx toCompanyFx(Recipe recipe) {
		final RecipeFx recipeFx = FxModelFactory.createRecipeFx(recipe);
		return recipeFx;
	}
}

package de.stl.saar.prog3.view.fx.model;

import de.stl.saar.prog3.factories.ModelFactory;
import de.stl.saar.prog3.model.interfaces.Recipe;
import de.stl.saar.prog3.utils.StringUtils;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class RecipeFx {
	private LongProperty recipeId;
	private StringProperty recipeName;
	private IntegerProperty personCount;
	private IntegerProperty healthRating;
	private SimpleBooleanProperty favorized;
	private StringProperty directions;
	private IntegerProperty time;
	private Recipe recipe;

	public RecipeFx() {
		recipeName = new SimpleStringProperty(StringUtils.EMPTY_STRING);
		recipeId = new SimpleLongProperty(0);
		personCount = new SimpleIntegerProperty(0);
		healthRating = new SimpleIntegerProperty(0);
		favorized = new SimpleBooleanProperty(false);
		directions = new SimpleStringProperty(StringUtils.EMPTY_STRING);
		time = new SimpleIntegerProperty(0);
		this.recipe = ModelFactory.createRecipe();
	}

	public RecipeFx(final Recipe recipe) {
		recipeName = new SimpleStringProperty(recipe.getRecipeName());
		recipeId = new SimpleLongProperty(recipe.getRecipeID());
		personCount = new SimpleIntegerProperty(recipe.getPersonCount());
		healthRating = new SimpleIntegerProperty(recipe.getHealthRating());
		favorized = new SimpleBooleanProperty(false);
		directions = new SimpleStringProperty(recipe.getDirections());
		time = new SimpleIntegerProperty(recipe.getTime());
		this.recipe = recipe;
	}

	public String getRecipeName() {
		return recipeName.get();
	}

	public void setRecipeName(String recipeName) {
		recipe.setRecipeName(recipeName);
		this.recipeName.set(recipeName);
	}

	public StringProperty recipeNameProperty() {
		return recipeName;
	}

	public Long getRecipeId() {
		return recipeId.get();
	}

	public LongProperty recipeIdProperty() {
		return recipeId;
	}

	public void setrecipeId(final long recipeId) {
		recipe.setRecipeID(recipeId);
		this.recipeId.set(recipeId);
	}

	public IntegerProperty personCountProperty() {
		return personCount;
	}

	public int getPersonCount() {
		return personCount.get();
	}

	public void setPersonCount(final int personCount) {
		recipe.setPersonCount(personCount);
		this.personCount.set(personCount);
	}

	public void setHealthRating(final int healthRating) {
		recipe.setHealthRating(healthRating);
		this.healthRating.set(healthRating);
	}

	public int getHealthRating() {
		return healthRating.get();
	}

	public void toggleFavorized() {
		recipe.setFavorized(!recipe.getFavorized());
		this.favorized.set(!recipe.getFavorized());
	}

	public boolean getFavorized() {
		return favorized.get();
	}

	public void setDirections(final String directions){
		recipe.setDirections(directions);
		this.directions.set(directions);
	}

	public String getDirections(){
		return directions.get();
	}

	public void setTime(final int time) {
		recipe.setTime(time);
		this.time.set(time);
	}

	public int getTime (){
		return time.get();
	}

	public Recipe getrecipe() {
		return recipe;
	}

	@Override
	public String toString() {
		return recipeName.get();
	}
}

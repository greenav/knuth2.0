package de.stl.saar.prog3.service.implementation;

import java.util.List;

import de.stl.saar.prog3.dao.interfaces.RecipeDao;
import de.stl.saar.prog3.factories.DaoFactory;
import de.stl.saar.prog3.model.interfaces.Recipe;
import de.stl.saar.prog3.service.interfaces.RecipeService;

public class RecipeServiceImpl implements RecipeService {
	private RecipeDao recipeDao;
	
	public RecipeServiceImpl() {
		recipeDao = DaoFactory.createRecipeDao();
	}
	
	@Override
	public Recipe saveNewRecipe(final Recipe recipe) {
		Recipe newRecipe = recipeDao.insertRecipe(recipe);
		return newRecipe;
	}
	
	@Override
	public List<Recipe> findAllRecipes() {
		return recipeDao.findAllRecipes();
	}
	
	@Override
	public Recipe upateRecipe(final Recipe updatedRecipe) {
		return recipeDao.updateRecipe(updatedRecipe);
	}
	
	@Override
	public Recipe deleteRecipe(final Recipe recipe) {
		return recipeDao.deleteRecipe(recipe.getRecipeID());
	}
}

package de.stl.saar.prog3.service.interfaces;

import java.util.List;

import de.stl.saar.prog3.model.interfaces.Recipe;

public interface RecipeService {

	Recipe saveNewRecipe(Recipe recipe);

	List<Recipe> findAllRecipes();

	Recipe upateRecipe(Recipe updatedRecipe);

	Recipe deleteRecipe(Recipe recipe);

}

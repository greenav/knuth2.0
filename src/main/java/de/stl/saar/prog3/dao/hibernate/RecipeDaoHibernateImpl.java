package de.stl.saar.prog3.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import de.stl.saar.prog3.dao.interfaces.RecipeDao;
import de.stl.saar.prog3.model.hibernate.RecipeHibernateImpl;
import de.stl.saar.prog3.model.interfaces.Recipe;
import de.stl.saar.prog3.utils.EntityManagerUtil;

public final class RecipeDaoHibernateImpl implements RecipeDao {
	@Override
	public Recipe deleteRecipe(final long recipeID) {
		final Recipe recipe = findRecipeByPrimaryKey(recipeID);
		return deleteRecipe(recipe);
	}
	
	@Override
	public Recipe deleteRecipe(final Recipe recipe) {
		EntityManager entityManager = null; 
		
		try {
			entityManager = EntityManagerUtil.getEntityManager();
			entityManager.getTransaction().begin();
			final Recipe recipeToDelete = (Recipe)entityManager.
					find(RecipeHibernateImpl.class, recipe.getRecipeID());
			if (recipeToDelete != null) {
				entityManager.remove(recipeToDelete);
			}
			entityManager.getTransaction().commit();
			return recipe;
		}
		catch(Exception e) {
			entityManager.getTransaction().rollback();
			e.printStackTrace();
		}
		finally {
			EntityManagerUtil.closeEntityManager(entityManager);
		}
		
		return null;
	}	

	@Override
	public Recipe findRecipeByPrimaryKey(final long recipeId) {
		EntityManager entityManager = null; 
		
		try {
			entityManager = EntityManagerUtil.getEntityManager();
			entityManager.getTransaction().begin();
			Recipe recipe = (Recipe)entityManager.
					find(RecipeHibernateImpl.class, recipeId);
			entityManager.getTransaction().commit();
			return recipe;
		}
		catch(Exception e) {
			entityManager.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
		finally {
			EntityManagerUtil.closeEntityManager(entityManager);
		}
	}
	
	@Override
	public Recipe findRecipeByRecipeName(final String recipeName) {
		final Recipe recipe;
		final String queryString = "SELECT r FROM "
				+ "RecipeHibernateImpl r "
				+ "WHERE r.recipeName = '" + recipeName + "'";
		EntityManager entityManager = null; 
		
		try {
			entityManager = EntityManagerUtil.getEntityManager();
			entityManager.getTransaction().begin();

			Query query = entityManager.createQuery(queryString);
			recipe = (Recipe)query.getSingleResult();
			entityManager.getTransaction().commit();
			return recipe;
		}
		catch (Exception e) {
			entityManager.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
		finally {
			EntityManagerUtil.closeEntityManager(entityManager);
		}
	}
	
	@Override
	public List<Recipe> findAllRecipes() {
		final List<Recipe> resultList;
		final String queryString = 
				"SELECT r FROM RecipeHibernateImpl r";

		EntityManager entityManager = null; 
		
		try {
			entityManager = EntityManagerUtil.getEntityManager();
			entityManager.getTransaction().begin();

			Query query = entityManager.createQuery(queryString);
			resultList = query.getResultList();
			entityManager.getTransaction().commit();
			return resultList;
		}
		catch (Exception e) {
			entityManager.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
		finally {
			EntityManagerUtil.closeEntityManager(entityManager);
		}
	}

	@Override
	public Recipe insertRecipe(final Recipe recipe) {
		EntityManager entityManager = null; 
		
		try {
			entityManager = EntityManagerUtil.
					getEntityManager();
			entityManager.getTransaction().begin();
			entityManager.persist(recipe);
			entityManager.getTransaction().commit();
		}
		catch(Exception e) {
			entityManager.getTransaction().rollback();
			e.printStackTrace();
		}
		finally {
			EntityManagerUtil.closeEntityManager(entityManager);
		}
		
		return recipe;
	}

	@Override
	public int countRecipes() {
		EntityManager entityManager = null; 
		
		try {
			entityManager = EntityManagerUtil.getEntityManager();
			entityManager.getTransaction().begin();

			final String queryString = "SELECT COUNT(r) FROM "
					+ "RecipeHibernateImpl r ORDER BY "
					+ "r.recipeName";
			Query query = entityManager.createQuery(queryString);
			final long recipeCount = (long)query.getSingleResult();
			return (int)recipeCount;
		}
		catch (Exception e) {
			entityManager.getTransaction().rollback();
			e.printStackTrace();
			return 0;
		}
		finally {
			EntityManagerUtil.closeEntityManager(entityManager);
		}
	}
	
	@Override
	public Recipe updateRecipe(final Recipe updatedRecipe) {
		EntityManager entityManager = null; 
		
		try {
			entityManager = EntityManagerUtil.getEntityManager();
			Recipe recipe = entityManager.
					find(RecipeHibernateImpl.class, 
							updatedRecipe.getRecipeID());
			entityManager.getTransaction().begin();
			
			recipe.setRecipeName(updatedRecipe.getRecipeName());
			recipe.setTime(updatedRecipe.getTime());
			recipe.setFavorized(updatedRecipe.getFavorized());
			recipe.setDirections(updatedRecipe.getDirections());
			recipe.setHealthRating(updatedRecipe.getHealthRating());
			recipe.setPersonCount(updatedRecipe.getPersonCount());
			
			//Recipe = updatedRecipe;
			entityManager.getTransaction().commit();
			return recipe;
		}
		catch(Exception e) {
			entityManager.getTransaction().rollback();
			e.printStackTrace();
		}
		finally {
			EntityManagerUtil.closeEntityManager(entityManager);
		}
		
		return null;
	}
	
}

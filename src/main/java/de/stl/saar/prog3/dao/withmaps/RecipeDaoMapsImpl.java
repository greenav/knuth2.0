package de.stl.saar.prog3.dao.withmaps;

import java.util.List;

import de.stl.saar.prog3.dao.interfaces.RecipeDao;
import de.stl.saar.prog3.dao.withmaps.database.Database;
import de.stl.saar.prog3.model.interfaces.Recipe;

public class RecipeDaoMapsImpl implements RecipeDao {
	private Database database;
	
	public RecipeDaoMapsImpl() {
		database = Database.getInstance();
	}
	
	@Override
	public Recipe deleteRecipe(final long recipeId){
		return database.deleteFromRecipe(recipeId);
	}

	@Override
	public Recipe findRecipeByPrimaryKey(final long recipeID) {
		return database.selectFromRecipe(recipeID);
	}
	
	@Override
	public List<Recipe> findAllRecipes() {
		return database.selectAllFromRecipe();
	}
	
	@Override
	public Recipe insertRecipe(final Recipe recipe) {
		return database.insertRecipe(recipe);
	}
	
	@Override
	public int countRecipes() {
		return database.selectCountFromRecipe();
	}

	@Override
	public Recipe updateRecipe(Recipe updatedRecipe) {
		return database.updateRecipe(updatedRecipe);
	}

	@Override
	public Recipe findRecipeByRecipeName(String recipeName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Recipe deleteRecipe(Recipe recipe) {
		
		return deleteRecipe(recipe.getRecipeID());
	}



}

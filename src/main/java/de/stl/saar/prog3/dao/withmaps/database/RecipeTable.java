package de.stl.saar.prog3.dao.withmaps.database;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.stl.saar.prog3.model.interfaces.Recipe;

public class RecipeTable {
	private Map<Long, Recipe> recipeTable;
	
	public RecipeTable() {
		recipeTable = new HashMap<>();
	}
	
	public Recipe createRecipe(final Recipe recipe) {
		long recipeCount = recipeTable.values().size();
		final Long recipeID = ++recipeCount;
		recipe.setRecipeID(recipeID);
		//final long RecipeID = Recipe.getRecipeID();
		recipeTable.put(recipeID, recipe);
		return recipe;

	}
	
	public Recipe saveRecipe(final Recipe recipe) {
		Recipe newRecipe = recipe;
		if (recipe.getRecipeID() == 0) {
			newRecipe = createRecipe(recipe);
		} else {
			recipeTable.replace(recipe.getRecipeID(), recipe);
		}
		
		return newRecipe;
	}

	public Recipe selectFromRecipe(long recipeID) {
		return recipeTable.get(recipeID);
	}

	public Recipe deleteFromRecipe(long recipeID) {
		final Recipe recipe = recipeTable.get(recipeID);
		recipeTable.remove(recipeID);
		return recipe;
	}

	public List<Recipe> selectAllFromRecipe() {
		final Collection<Recipe> recipeCollection = recipeTable.values();
		final Recipe[] recipeArray = new Recipe[recipeCollection.size()];
		recipeCollection.toArray(recipeArray);
		return Arrays.asList(recipeArray);
	}
	
	/*public Recipe selectRecipeByRecipeName(final String RecipeName) {
		final List<Recipe> Recipes = selectAllFromRecipe();
		
		for(final Recipe Recipe: Recipes) {
			if (StringUtils.areStringsEqual(Recipe.getRecipeName(), RecipeName)) {
				return Recipe;
			}
		}
		
		return null;
	}*/

	public int selectCountFromRecipe() {
		return recipeTable.size();
	}
}

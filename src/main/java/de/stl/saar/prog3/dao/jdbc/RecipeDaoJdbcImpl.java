package de.stl.saar.prog3.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import de.stl.saar.prog3.dao.interfaces.RecipeDao;
import de.stl.saar.prog3.factories.ModelFactory;
import de.stl.saar.prog3.model.interfaces.Recipe;
import de.stl.saar.prog3.utils.JdbcUtils;

public final class RecipeDaoJdbcImpl implements RecipeDao {
	private static final String COL_RECIPE_ID = "RecipeID";
	private static final String COL_RECIPE_NAME = "RecipeName";
	private static final String COL_COUNT = "RecipeCount";
	private static final String COL_TIME = "Time";
	private static final String COL_HEALTHRATING = "HealthRating";
	private static final String COL_PERSONCOUNT = "PersonCount";
	private static final String COL_FAVORIZED = "Favorized";
	private static final String COL_DIRECTIONS ="Directions";

	
	@Override
	public Recipe deleteRecipe(final long recipeID) {
		Connection connection = null;
		try {
			final Recipe deletedRecipe = findRecipeByPrimaryKey(recipeID);
			connection = JdbcUtils.getConnection();			
			final Statement statement = connection.createStatement();			
			final String queryString = "DELETE from Recipe WHERE "
									+ "RecipeId = " + recipeID;
			statement.executeUpdate(queryString);
			return deletedRecipe;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		
		return null;
	}

	@Override
	public Recipe findRecipeByPrimaryKey(final long recipeID) {
		Connection connection = null;
		try {
			connection = JdbcUtils.getConnection();			
			final String queryString = "SELECT * from Recipe WHERE "
									 + "RecipeID = ?";
			final PreparedStatement preparedStatement = connection.
					prepareStatement(queryString);
			preparedStatement.setLong(1, recipeID);
			final ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			final Recipe Recipe = createRecipeFromResultSet(resultSet);
			return Recipe;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public Recipe findRecipeByRecipeName(final String recipeName) {
		Connection connection = null;
		try {
			connection = JdbcUtils.getConnection();			
			final Statement statement = connection.createStatement();			
			final String queryString = "SELECT * from Recipe WHERE "
									 + "RecipeName = '" + recipeName 
									 + "'";
			final ResultSet resultSet = statement.executeQuery(queryString);
			resultSet.next();
			final Recipe recipe = createRecipeFromResultSet(resultSet);
			return recipe;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	private Recipe createRecipeFromResultSet(final ResultSet resultSet) throws SQLException {
		final long recipeID = resultSet.getLong(COL_RECIPE_ID);
		final String recipeName = resultSet.getString(COL_RECIPE_NAME);
		 
		if (resultSet.wasNull()) {
			System.out.println("Beim Rezept mit der ID " + recipeID + 
					" ist kein Name angegeben!");
		}

		final int time = resultSet.getInt(COL_TIME);
		final int healthRating = resultSet.getInt(COL_HEALTHRATING);
		final int personCount = resultSet.getInt(COL_PERSONCOUNT);
		final boolean favorized = resultSet.getBoolean(COL_FAVORIZED);
		final String directions = resultSet.getString(COL_DIRECTIONS);

		final Recipe recipe = ModelFactory.createRecipe(recipeID, recipeName, healthRating, personCount, favorized, directions, time );
		return recipe;
	}

	@Override
	public List<Recipe> findAllRecipes() {
		Connection connection = null;
		final List<Recipe> RecipeList = new ArrayList<>();
		try {
			connection = JdbcUtils.getConnection();
			final Statement statement = connection.
					createStatement();
			final String queryString = "SELECT * FROM Recipe";
			final ResultSet resultSet = statement.
					executeQuery(queryString);				
			
			while (resultSet.next()) {
				final Recipe Recipe = createRecipeFromResultSet(resultSet);
				RecipeList.add(Recipe);
			}						
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return RecipeList;
	}

	@Override
	public Recipe insertRecipe(final Recipe recipe) {
		Connection connection = null;
		try {
			recipe.setRecipeID(countRecipes() + 1);
			connection = JdbcUtils.getConnection();			
			final Statement statement = connection.createStatement();			
			final String queryString = "INSERT INTO Recipe values (" 
								    + recipe.getRecipeID() + ", '" 
								    + recipe.getRecipeName() + "', "
									;
			statement.executeUpdate(queryString);	
			return recipe;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		
		return null;
	}

	@Override
	public int countRecipes() {
		Connection connection = null;
		
		try {
			connection = JdbcUtils.getConnection();
			final Statement statement = connection.createStatement();
			final String queryString = "SELECT COUNT(*) as "
					+ COL_COUNT	+ " FROM Recipe";
			final ResultSet resultSet = statement.executeQuery(queryString);
			resultSet.next();
			return resultSet.getInt(COL_COUNT);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return 0;
	}

	@Override
	public Recipe updateRecipe(final Recipe updatedRecipe) {
		Connection connection = null;
		try {
			connection = JdbcUtils.getConnection();			
			final Statement statement = connection.createStatement();			
			final String queryString = "UPDATE Recipe SET "  
					+ COL_RECIPE_NAME + " = '" + updatedRecipe.getRecipeName() + "', " 
					+ " WHERE RecipeID = " + updatedRecipe.getRecipeID(); 
			statement.executeUpdate(queryString);
			return updatedRecipe;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	public Recipe deleteRecipe(Recipe recipe) {
		return deleteRecipe(recipe.getRecipeID());

	}


}

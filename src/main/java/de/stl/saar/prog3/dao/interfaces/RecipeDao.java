package de.stl.saar.prog3.dao.interfaces;

import java.util.List;

import de.stl.saar.prog3.model.interfaces.Recipe;

public interface RecipeDao {

	Recipe deleteRecipe(long recipeId);

	Recipe findRecipeByPrimaryKey(long recipeId);

	List<Recipe> findAllRecipes();

	Recipe insertRecipe(Recipe recipe);

	int countRecipes();

	Recipe updateRecipe(Recipe updatedRecipe);

	Recipe findRecipeByRecipeName(String recipeName);

	Recipe deleteRecipe(Recipe recipe);


	
}



package de.stl.saar.prog3.dao.withmaps.database;

import java.util.List;

import de.stl.saar.prog3.factories.ModelFactory;
import de.stl.saar.prog3.model.interfaces.Recipe;


public class Database {
	private RecipeTable recipeTable;
	private static Database database;
	
	private Database() {
		recipeTable = new RecipeTable();
		initializeTestData();
	}
	
	private void initializeTestData() {
		initializeRecipes();
	}
	
	private void initializeRecipes() {
		final Recipe pizza = ModelFactory.createRecipe();
		final Recipe feta = ModelFactory.createRecipe();
		final Recipe pasta = ModelFactory.createRecipe();
		recipeTable.createRecipe(pizza);
		recipeTable.createRecipe(feta);
		recipeTable.createRecipe(pasta);
	}
	
	
	public static Database getInstance() {
		if (database == null) {
			database = new Database();
		}
		
		return database;
	}
	


	public Recipe deleteFromRecipe(long recipeID) {
		return recipeTable.deleteFromRecipe(recipeID);
	}
	
	public Recipe selectFromRecipe(long recipeID) {
		return recipeTable.selectFromRecipe(recipeID);
	}

	public List<Recipe> selectAllFromRecipe() {
		return recipeTable.selectAllFromRecipe();
	}

	public Recipe insertRecipe(Recipe recipe) {
		return recipeTable.createRecipe(recipe);
	}

	public int selectCountFromRecipe() {
		return recipeTable.selectCountFromRecipe();
	}

	public Recipe updateRecipe(Recipe updatedRecipe) {
		return recipeTable.saveRecipe(updatedRecipe);
	}
	
	
	public RecipeTable getRecipeTable() {
		return recipeTable;
	}
	

}

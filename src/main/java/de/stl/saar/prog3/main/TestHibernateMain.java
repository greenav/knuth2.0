package de.stl.saar.prog3.main;

import java.util.List;

import javax.persistence.EntityManager;

import de.stl.saar.prog3.dao.hibernate.RecipeDaoHibernateImpl;
import de.stl.saar.prog3.dao.interfaces.RecipeDao;
import de.stl.saar.prog3.factories.ModelFactory;
import de.stl.saar.prog3.model.hibernate.RecipeHibernateImpl;
import de.stl.saar.prog3.model.interfaces.Recipe;
import de.stl.saar.prog3.utils.EntityManagerUtil;

public class TestHibernateMain {
	private RecipeDao recipeDao;

	public static void main(String[] args) {
		final TestHibernateMain testHibernateMain = new TestHibernateMain();
		testHibernateMain.testPersistent();
	}
	
	public void insertNewRecipe() {
		System.out.println("Anzahl Rezepte vor Einfügen: " + recipeDao.countRecipes());
		Recipe recipe = new RecipeHibernateImpl();
		recipe.setRecipeName("Pizza");
		recipeDao.insertRecipe(recipe);
		
		System.out.println("Anzahl Rezepte nach Einfügen: " + recipeDao.countRecipes());
		System.out.println("Primärschlüsselwert: " + recipe.getRecipeID());
	}
	
	public void testJpa() {
		Recipe recipe = new RecipeHibernateImpl();
		recipe.setRecipeName("Schoki");
		RecipeDao recipeDao = new RecipeDaoHibernateImpl();
		recipeDao.insertRecipe(recipe);
		
		System.out.println(recipeDao.countRecipes());
	}
	
	public TestHibernateMain() {
		recipeDao = new RecipeDaoHibernateImpl();
	}
	
	public void testPersistent() {
		Recipe recipe = new RecipeHibernateImpl();
		recipe.setRecipeName("Pizza");
		
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(recipe);
		
		Recipe pizza = entityManager.find(RecipeHibernateImpl.class, 1l);
		System.out.println(pizza.getRecipeName());
		
		recipe.setRecipeName("Feta");
		Recipe feta = entityManager.find(RecipeHibernateImpl.class, 1l);
		System.out.println(feta.getRecipeName());
		
		recipe.setRecipeName("Pasta");
		
		Recipe pasta = entityManager.find(RecipeHibernateImpl.class, 1l);
		System.out.println(pasta.getRecipeName());
		
		EntityManagerUtil.closeEntityManager(entityManager);
	}
	
	private void initializeDatabase() {
		
		final Recipe pizza = ModelFactory.createRecipe();
		recipeDao.insertRecipe(pizza);
		
		
		final Recipe pasta = ModelFactory.createRecipe();
		recipeDao.insertRecipe(pasta);
		
		
	}
}

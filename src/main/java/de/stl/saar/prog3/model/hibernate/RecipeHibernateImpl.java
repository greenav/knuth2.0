package de.stl.saar.prog3.model.hibernate;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



import de.stl.saar.prog3.model.interfaces.Recipe;


@Entity
@Table(name = "recipe")
public class RecipeHibernateImpl implements Recipe {
	private long recipeID;
	private String recipeName;
	private int time;
	private int healthRating;
	private boolean favorized;
	private int personCount;
	private String directions;
	
	
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getRecipeID() {
		return recipeID;
	}
	
	@Override
	public void setRecipeID(long recipeID) {
		this.recipeID = recipeID;
	}
	
	@Override
	@Column(name = "RecipeName", 
		nullable = false)
	public String getRecipeName() {
		return recipeName;
	}
	
	@Override
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	
	@Override
	public int getTime() {
		return time;
	}
	
	@Override
	public void setTime(int time) {
		this.time = time;
	}
	
	@Override
	public String toString() {
		return recipeName;
	}
	
	@Override
	public int getHealthRating() {
		return healthRating;
	}
	
	@Override
	public void setHealthRating(int healthRating) {
		this.healthRating = healthRating;
	}
	
	@Override
	public boolean getFavorized() {
		return favorized;
	}

	@Override
	public void setFavorized(boolean favorized) {
		this.favorized = favorized;
		
	}

	@Override
	public int getPersonCount() {
		return personCount;
		
	}

	@Override
	public void setPersonCount(int personCount){
		this.personCount = personCount;
	}

	@Override
	public String getDirections(){
		return directions;
	}

	@Override
	public void setDirections(String directions){
		this.directions = directions;
	}

	
	//@Override
	//@OneToMany(targetEntity = PersonHibernateImpl.class,
	//		   mappedBy = "Recipe")
	//public List<Person> getPersons() {
	//	return persons;
	//}
	
	//@Override
	//public void setPersons(List<Person> persons) {
	//	this.persons = persons;
	//}
}

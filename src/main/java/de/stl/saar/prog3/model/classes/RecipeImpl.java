package de.stl.saar.prog3.model.classes;



import de.stl.saar.prog3.model.interfaces.Recipe;


public class RecipeImpl implements Recipe {
	private long recipeID;
	private String recipeName;
	private int time;
	private int healthRating;
	private boolean favorized;
	private int personCount;
	private String directions;

	
	
	@Override
	public long getRecipeID() {
		return recipeID;
	}
	
	@Override
	public void setRecipeID(long recipeID) {
		this.recipeID = recipeID;
	}
	
	@Override
	public String getRecipeName() {
		return recipeName;
	}
	
	@Override
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	
	@Override
	public int getTime() {
		return time;
	}
	
	@Override
	public void setTime(int time) {
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "ID: " + recipeID + " Rezeptname: " + recipeName + " Zeit: " + time;
	}

	@Override
	public int getHealthRating() {
		return healthRating;
	}

	@Override
	public void setHealthRating(int healthRating) {
		this.healthRating = healthRating;	
	}

	@Override
	public boolean getFavorized() {
		return favorized;
	}

	@Override
	public void setFavorized(boolean favorized) {
		this.favorized = favorized;
		
	}

	@Override
	public int getPersonCount() {
		return personCount;
		
	}

	@Override
	public void setPersonCount(int personCount){
		this.personCount = personCount;
	}

	@Override
	public String getDirections(){
		return directions;
	}

	@Override
	public void setDirections(String directions){
		this.directions = directions;
	}
}

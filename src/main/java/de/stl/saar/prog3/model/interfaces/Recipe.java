package de.stl.saar.prog3.model.interfaces;





public interface Recipe {

	void setTime(int time);

	int getTime();

	void setRecipeName(String recipeName);

	String getRecipeName();

	void setRecipeID(long recipeID);

	long getRecipeID();

	int getHealthRating();

	void setHealthRating(int healthRating);

	boolean getFavorized();

	void setFavorized(boolean favorized);

	String getDirections();

	void setDirections(String directions);

	int getPersonCount();

	void setPersonCount(int personCount);



}

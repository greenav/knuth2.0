package de.stl.saar.prog3.factories;

import de.stl.saar.prog3.model.hibernate.RecipeHibernateImpl;
import de.stl.saar.prog3.model.interfaces.Recipe;

public class ModelFactory {

	public static Recipe createRecipe() {
		final Recipe recipe = new RecipeHibernateImpl();
		return recipe;
	}

	public static Recipe createRecipe(long recipeID, String recipeName) {
		final Recipe recipe = new RecipeHibernateImpl();
		recipe.setRecipeID(recipeID);
		recipe.setRecipeName(recipeName);
		return recipe;
	}
	
	public static Recipe createRecipe(long recipeId, String recipeName, int healthRating, int personCount, boolean favorized, String directions, int time) {
		final Recipe recipe = new RecipeHibernateImpl();
		recipe.setRecipeID(recipeId);
		recipe.setRecipeName(recipeName);
		recipe.setHealthRating(healthRating);
		recipe.setPersonCount(personCount);
		recipe.setFavorized(favorized);
		recipe.setDirections(directions);
		recipe.setTime(time);
		return recipe;
	}
	
	
}

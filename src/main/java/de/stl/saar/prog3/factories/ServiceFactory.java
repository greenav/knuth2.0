package de.stl.saar.prog3.factories;

import de.stl.saar.prog3.service.implementation.RecipeServiceImpl;
import de.stl.saar.prog3.service.interfaces.RecipeService;


public class ServiceFactory {


	public static RecipeService createRecipeService() {
		return new RecipeServiceImpl();
	}
}

package de.stl.saar.prog3.factories;

import java.util.ArrayList;
import java.util.List;

import de.stl.saar.prog3.model.interfaces.Recipe;
import de.stl.saar.prog3.view.fx.model.RecipeFx;


public class FxModelFactory {

	
	public static RecipeFx createRecipeFx() {
		final RecipeFx recipeFx = new RecipeFx();
		return recipeFx;
	}
	
	public static RecipeFx createRecipeFx(final Recipe recipe) {
		final RecipeFx recipeFx = new RecipeFx(recipe);
		return recipeFx;
	}

	public static List<RecipeFx> createRecipeFxList(List<Recipe> recipes) {
		final List<RecipeFx> recipeFxList = new ArrayList<>();
		for (final Recipe recipe: recipes) {
			final RecipeFx recipeFx = createRecipeFx(recipe);
			recipeFxList.add(recipeFx);
		}
		
		return recipeFxList;
	}
}

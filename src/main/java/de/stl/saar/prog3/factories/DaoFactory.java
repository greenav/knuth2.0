package de.stl.saar.prog3.factories;

import de.stl.saar.prog3.dao.hibernate.RecipeDaoHibernateImpl;
import de.stl.saar.prog3.dao.interfaces.RecipeDao;


public class DaoFactory {
	
	public static RecipeDao createRecipeDao() {
		return new RecipeDaoHibernateImpl();
	}
}

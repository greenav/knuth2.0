package de.stl.saar.prog3.facades;

import java.util.List;

import de.stl.saar.prog3.factories.ServiceFactory;
import de.stl.saar.prog3.model.interfaces.Recipe;
import de.stl.saar.prog3.service.interfaces.RecipeService;


public class ServiceFacade {
	private RecipeService recipeService;
	private static ServiceFacade serviceFacade;
	
	private ServiceFacade() {
		this.recipeService = ServiceFactory.createRecipeService();
	}
	
	
	public Recipe saveNewRecipe(final Recipe recipe) {
		return recipeService.saveNewRecipe(recipe);
	}
	
	public static ServiceFacade getInstance() {
		if (serviceFacade == null) {
			serviceFacade = new ServiceFacade();
		}
		
		return serviceFacade;
	}



	public List<Recipe> findAllRecipes() {
		return recipeService.findAllRecipes();
	}
}

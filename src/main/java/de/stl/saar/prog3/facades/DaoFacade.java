package de.stl.saar.prog3.facades;

import java.util.List;

import de.stl.saar.prog3.dao.interfaces.RecipeDao;
import de.stl.saar.prog3.factories.DaoFactory;
import de.stl.saar.prog3.model.interfaces.Recipe;


public class DaoFacade {
	private RecipeDao recipeDao;
	private static DaoFacade daoFacade;
	
	private DaoFacade() {
		this.recipeDao = DaoFactory.createRecipeDao();
	}
	
	public static DaoFacade getInstance() {
		if (daoFacade == null) {
			daoFacade = new DaoFacade();
		}
		
		return daoFacade;
	}
	
	
	public Recipe deleteRecipe(final int recipeId) {
		return recipeDao.deleteRecipe(recipeId);
	}

	public Recipe findRecipeByPrimaryKey(final long recipeId) {
		return recipeDao.findRecipeByPrimaryKey(recipeId);
	}

	public List<Recipe> findAllRecipes() {
		return recipeDao.findAllRecipes();
	}

	public Recipe insertRecipe(final Recipe Recipe) {
		return recipeDao.insertRecipe(Recipe);
	}

	public int countRecipes() {
		return recipeDao.countRecipes();
	}
}


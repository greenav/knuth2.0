package de.stl.saar.prog3.utils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import de.stl.saar.prog3.constants.DatabaseConstants;
import de.stl.saar.prog3.dao.withmaps.database.RecipeTable;
import de.stl.saar.prog3.dao.withmaps.database.Database;
import de.stl.saar.prog3.enums.DatabaseType;
import de.stl.saar.prog3.model.interfaces.Recipe;


/**
 * Initialisiert die Datenbank bei der Verwendung von JDBC mit Testdaten.
 * @author christopher
 *
 */
public class JdbcDatabaseInitializationUtil {
	private static RecipeTable recipeTable;
	
	/**
	 * Initialisiert die Datenbank mit Testdaten entweder fuer MySQL oder
	 * fuer HSQLDB. Die Information, welche Datenbank verwendet wird, wird
	 * aus {@see de.stl.saar.prog3.utils.JdbcUtils} genommen.
	 */
	public static void initializeDatabase() {
		final Database database = Database.getInstance();
		recipeTable = database.getRecipeTable();

		
		deleteAllTables();
		
		final DatabaseType databaseType = JdbcUtils.ACTUAL_DATABASE;
		if (databaseType.equals(DatabaseType.MYSQL)) {
			createAllTablesAndTestDataMySql();
		} else if (databaseType.equals(DatabaseType.HSQLDB)) {
			createAllTablesAndTestDataHSQLDB();
		}
	}
	
	/**
	 * Checks if a table exists in a MYSQL database.
	 * @param tableName The name of the table we want to check.
	 * @return True if the table not exists false otherwise. 
	 */
	private static boolean doesTableNotExistMySQL(final String tableName) {
		Connection connection = null;
		
		try {
			connection = JdbcUtils.getConnection();
			
			DatabaseMetaData meta = connection.getMetaData();
			ResultSet res = meta.getTables(null, null, tableName, 
					new String[] {"TABLE"});
			if (res.next()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;
	}
	
	/**
	 * Erzeugt saemtliche Tabellen der Anwendung fuer MySQL und schreibt die Testdaten hinein.
	 */
	private static void createAllTablesAndTestDataMySql() {
		Connection connection = null;
		final List<String> createStatements = DatabaseConstants.getCreateStatementsForAllMySqlTables();
		
		try {
			connection = JdbcUtils.getConnection();
			
			for (final String aStatement: createStatements) {
				final Statement statement = connection.createStatement();
				final int posTableNameEnd = aStatement.indexOf("(");
				final String tableName = aStatement.substring(12, posTableNameEnd).trim();
				if (doesTableNotExistMySQL(tableName)) {
					statement.executeUpdate(aStatement);
				}
			}
			createTestdataForRecipes();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Erzeugt saemtliche Tabellen der Anwendung fuer HSQLDB und schreibt die Testdaten hinein.
	 */
	private static void createAllTablesAndTestDataHSQLDB() {
		Connection connection = null;
		final List<String> createStatements = DatabaseConstants.getCreateStatementsForAllHsqlDbTables();
		
		try {
			connection = JdbcUtils.getConnection();
			
			for (final String aStatement: createStatements) {
				final Statement statement = connection.createStatement();
				statement.executeUpdate(aStatement);
			}
			createTestdataForRecipes();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Loescht alle Tabellen mit dem Kommando DROP IF EXIST
	 */
	private static void deleteAllTables() {
		Connection connection = null;
		
		try {
			connection = JdbcUtils.getConnection();
			final Statement deleteStatementRecipe = connection.createStatement();
			deleteStatementRecipe.executeUpdate("DROP TABLE IF EXISTS Recipe");
			
			final Statement deleteStatementEquipment = connection.createStatement();
			deleteStatementEquipment.executeUpdate("DROP TABLE IF EXISTS equipment");
			
			final Statement deleteStatementPerson = connection.createStatement();
			deleteStatementPerson.executeUpdate("DROP TABLE IF EXISTS person");
			
			final Statement deleteStatementPersonHasEquipment = connection.createStatement();
			deleteStatementPersonHasEquipment.executeUpdate("DROP TABLE IF EXISTS personhasequipment");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Erzeugt die Testdaten fuer die Firmen.
	 */
	private static void createTestdataForRecipes() {
		Connection connection = null;
		Statement statement = null;
		
		try {
			// Einfuegen mittels Batch-Statement.
			connection = JdbcUtils.getConnection();
			statement = connection.createStatement();
			final List<Recipe> recipes = recipeTable.selectAllFromRecipe();
			
			for (final Recipe recipe: recipes) {
				String sqlStatement = "INSERT INTO Recipe values ("
						+ recipe.getRecipeID() + ", '" 
						+ recipe.getRecipeName() + "', "
						+ recipe.getTime() + ")";
				statement.addBatch(sqlStatement);
			}
			statement.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();					
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	

	

}

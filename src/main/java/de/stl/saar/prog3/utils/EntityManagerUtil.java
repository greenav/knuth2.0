package de.stl.saar.prog3.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtil {
	private static final EntityManagerFactory 
		entityManagerFactory;
	private static final String NAME_OF_PERSISTANCE_UNIT = 
		"hibernate-persistence";
	
	static {
		entityManagerFactory = Persistence.
			createEntityManagerFactory(NAME_OF_PERSISTANCE_UNIT);
	}
	
	public static EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	public static void closeEntityManager(
			final EntityManager entityManager) {
		entityManager.close();
	}
}

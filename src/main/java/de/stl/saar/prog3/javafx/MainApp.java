package de.stl.saar.prog3.javafx;

/**
 * Class to start the "Bachelorarbeit-Tool" application.
 */
public class MainApp {
	public static void main(String[] args) {
		final MainApp testHibernateMain = new MainApp();
		//testHibernateMain.testPersistent();
	}
	
	public MainApp() {
	}
	
	/*public void testPersistent() {
		Company company = new CompanyHibernateImpl();
		company.setCompanyName("HTW");
		
		Session session = null;
		session = HibernateUtil.getSession();
		session.beginTransaction();
		session.saveOrUpdate(company);
		
		Company htw = session.get(CompanyHibernateImpl.class, 1l);
		System.out.println(htw.getCompanyName());
		
		company.setCompanyName("Uni");
		Company uni = session.get(CompanyHibernateImpl.class, 1l);
		System.out.println(uni.getCompanyName());
		
		company.setCompanyName("DFKI");
		
		Company dfki = session.get(CompanyHibernateImpl.class, 1l);
		System.out.println(dfki.getCompanyName());
		
		session.close();
	}*/
}

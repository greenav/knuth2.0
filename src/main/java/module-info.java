module de.stl.saar.prog3 {
    requires javafx.controls;
	requires javafx.fxml;
	requires java.desktop;
	requires transitive javafx.graphics;
	requires javafx.base; 
	requires java.sql;
	//requires hibernate.jpa;
	requires java.persistence;
    exports de.stl.saar.prog3.view.fx.controllers;
    exports de.stl.saar.prog3.view.fx.model;
    exports de.stl.saar.prog3.main;
    exports de.stl.saar.prog3.model.hibernate;
    opens de.stl.saar.prog3.view.fx.controllers to javafx.fxml, javafx.graphics;
    opens de.stl.saar.prog3.main to javafx.graphics;
    opens de.stl.saar.prog3.dao.hibernate;
    opens de.stl.saar.prog3.model.hibernate;
    opens de.stl.saar.prog3.model.interfaces;
}